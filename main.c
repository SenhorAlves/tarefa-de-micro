/*
    UFC - Universidade Federal do Ceará
    Professor: Jardel
    Sistemas Microprocessados
    Nome: Aluísio Alves da Cunha
    Matrícula: 409878
    17/08/2019
*/

#include <stdio.h>
#include <stdlib.h>

unsigned char ReadBit(unsigned long *p, unsigned char bitn) { 
    if (*p & (1<<bitn)){
        return 1;
    }
    else {
        return 0;
    }
}

void SetBit(unsigned long*p, unsigned char bitn) {
    *p = *p | (1 << bitn);
}

void ResetBit(unsigned long *p, unsigned char bitn) {
    *p = *p & ~(1 << bitn);
}

void ToggleBit(unsigned long *p, unsigned char bitn) {
    *p = *p ^ (1 << bitn);
}


unsigned long ReadBitSlice(unsigned long *p, unsigned char bitstart, unsigned char bitend) {
    return (*p >> bitstart) & ( (1 <<  bitend - bitstart) - 1);
}

unsigned char IsLittleEndian (void) {
    unsigned long teste = 1;
    return (unsigned char) *(&teste);
}

void PrintBits(unsigned long *p) {
    for(int i = 31; i >= 0 ; i--){
        printf("%d", ReadBit(p, i));
    }
    printf("\n");
}

int main () {
    unsigned long x = 111118734;
    unsigned long *p = &x;
    PrintBits(p);
    unsigned long y = ReadBitSlice(p, 5, 8);
    unsigned long *u = &y;
    PrintBits(u);
    return 0;
}

